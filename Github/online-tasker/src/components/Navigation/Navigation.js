import React from 'react';
import './Navigation.scss';
import {NavLink} from "react-router-dom";
import logo from '../../img/airshield.png';

export default class Navigation extends React.Component {

    render() {
        return (
            <header>
                <div className="header-container">
                    <NavLink to=''><img alt={"logo"} className="img--logo" src={logo}/>
                    </NavLink>
                    <div className="menu-container">
                        <div className="menu">
                            <NavLink to='' className="button-xs">Post a Task</NavLink>
                            <nav>
                                <ul>
                                    <li><NavLink to='' className="browser-task">Browser Task</NavLink></li>
                                    <li><NavLink to='' className="browser-task">How it works</NavLink></li>
                                </ul>
                            </nav>
                        </div>
                        <div className="menu-user relative">
                            <NavLink to="" className="button--light button-xs">Become a Tasker</NavLink>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

