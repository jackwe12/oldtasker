import React from "react";
import "./CardTask.scss"
import {Link} from "react-router-dom";
import {commerce, image, name, hacker} from 'faker';
class CardTask extends React.Component {
    render() {
        return (
            <Link className="task__card task-card-animation" >
                <span>{hacker.ingverb()}</span>
                <div className="task__card--main-container">
                    <img alt={"decoration"}
                        src={image.avatar()}
                        className="life_moments_task__TaskAvatar-sc-153tdrc-8 dpkWtO"/>
                    <h5 className="life_moments_task__TaskTitle-sc-153tdrc-2 dCMZFf">{name.firstName()}</h5>
                    <p className="life_moments_task__TaskPrice-sc-153tdrc-3 fDkpVx">{'$' + commerce.price()}</p>
                </div>
                <div className="task__card--rate-container">
                  <svg height="16" width="16" class="task__card--star" viewBox="0 0 24 24"><path class="task__card--star" d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path></svg>
                  <div>5 stars</div>
                </div>
            </Link>
        )
    }
}

export default CardTask;

