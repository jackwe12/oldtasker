import React from 'react';
import './Footer.scss';
import {NavLink} from "react-router-dom";
import logo from '../../img/icon.svg';
import google from '../../img/google-play.svg';
import apple from '../../img/apple-store.svg';

export default class Footer extends React.Component {
    render() {
        return (
            <footer>
                <div className="menu-hierarchy">
                    <div className="menu-folder dynamic expanded">
                        <div className="menu-folder-control showing">
                            <a href="/no-page" className="link">Discover</a>
                        </div>
                        <div className="menu-folder-items showing">
                            <a className="link" href="/how-it-works/">How it works</a>
                            <a className="link" href="/earn-money/">Earn money</a>
                            <a className="link" href="/">New users FAQ</a>
                        </div>
                    </div>
                    <div className="menu-folder dynamic">
                        <div className="menu-folder-control showing">
                            <a href="/no-page" className="link">Company</a>
                        </div>
                        <div className="menu-folder-items">
                            <a className="link" href="/about/">About us</a>
                            <a className="link" href="/careers/">Careers</a>

                            <a className="link" href="/terms/">Terms &amp; conditions</a>
                            <a className="link" target="_blank" rel="noopener noreferrer"
                               href="/">Blog</a>
                            <a className="link" target="_blank" rel="noopener noreferrer"
                               href="/">Contact
                                us</a>
                            <a className="link" href="/privacy/">Privacy policy</a>
                        </div>
                    </div>
                    <div className="menu-folder dynamic">
                        <div className="menu-folder-control showing">
                            <a href="/no-page" className="link">Existing Members</a>
                        </div>
                        <div className="menu-folder-items">
                            <NavLink className={"link"} to='/view-tasks'>Browse tasks</NavLink>
                            {!this.props.isAuth && <NavLink className={"link"} to='/login'>Login</NavLink>}
                            <a className="link" href="/">Support centre</a>
                            <a className="link" target="_blank" rel="noopener noreferrer"
                               href="/">Merchandise</a>
                        </div>
                    </div>
                    <div className="menu-folder dynamic">
                        <div className="menu-folder-control showing">
                            <a href="/no-page" className="link">Popular Categories</a>
                        </div>
                        <div className="menu-folder-items">
                            <a className="link" href="/create-task/clean/">Cleaning Services</a>
                            <a className="link" href="/categories/">All Services</a>
                        </div>
                    </div>
                    <div className="menu-folder dynamic">
                        <div className="menu-folder-control showing">
                            <a href="/no-page" className="link">Partners</a>
                        </div>
                        <div className="menu-folder-items">
                            <a className="link" href="https://www.7plus.com.au/">7Plus</a>
                            <a className="link" href="https://www.newidea.com.au/">New Idea</a>
                            <a className="link" href="https://thewest.com.au">The West Australian</a>
                            <a className="link" href="https://www.marieclaire.com.au/">Marie Claire</a>
                            <a className="link" href="https://www.perthnow.com.au/">PerthNow</a>
                            <a className="link" href="https://www.bhg.com.au/">Better Homes and Gardens</a>
                            <a className="link" href="https://www.girlfriend.com.au/partners">Girlfriend</a>
                            <a className="link" href="http://www.sevenwestmedia.com.au/about-us/">Seven West Media</a>
                        </div>
                    </div>
                </div>
                <div className="footer-links">
                    <a className="inline-block" rel="noopener noreferrer" target="_blank"
                       href="/">
                        <img src={google} alt="Google play"/>
                    </a>
                    <a className="inline-block" rel="noopener noreferrer" target="_blank"
                       href="/">
                        <img src={apple} alt="Apple store"/>
                    </a>
                </div>
                <div className="footer-pty-ltd text-center">
                    <div className="container--footer">
                        <img src={logo} alt={"logo"} className={"footer-icon"}/>
                        <span className="company-details vertical-middle">Shield Pty. Ltd 2011-2019©, All rights reserved</span>
                    </div>
                </div>
            </footer>
        );
    }
}
