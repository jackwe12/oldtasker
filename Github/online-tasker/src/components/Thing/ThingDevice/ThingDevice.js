import React from 'react';
import './ThingDevice.scss';

function ThingDevice(props) {

    return (
        <div className="thing flex space-around">
            <div className="home-thing-container">
                <div className="home-thing-bg">

                    <div className="home-thing-bg-device-wrapper">
                        <div className="home-thing-bg-device-left"></div>
                        <div className="home-thing-bg-device-right"></div>
                        <div className="home-thing-bg-device-padLock"></div>
                        <div className="home-thing-bg-device-light-1 "></div>
                        <div className="home-thing-bg-device-light-2"></div>
                        <div className="home-thing-bg-device-light-3"></div>
                    </div>
                </div>
            </div>

            <div className="home-thing-content">
                <h4>{props.title}</h4>
                <p>{props.content}</p>
            </div>
        </div>
    );
}

export default ThingDevice;

