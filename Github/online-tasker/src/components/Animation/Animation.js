let preScrollPosition = window.pageYOffset;


//whenever scroll occurs, fire the function
window.onscroll = function(){
    let currentScrollPosition = window.pageYOffset;
    if (currentScrollPosition>60){
      controlNavBar();
    }


    // animation event
    const animation=["fade","zoom","slide"];
    //find out all the animation element by className, which is a collection
    const animation_collections=animation.map(ani=>document.getElementsByClassName(ani));
    this.Array.from(animation_collections).forEach((ani_coll,i)=>{
        this.Array.from(ani_coll).forEach((ani_ele)=>{
        //the position of page bottom
          let currentBottomPosition = currentScrollPosition + window.innerHeight;
        //the position of element
          // let currentElementPosition = getOffsetTop(ani_ele)+ani_ele.offsetHeight;
          let currentElementPosition = getOffsetTop(ani_ele)+ani_ele.offsetHeight/2;
          if(currentBottomPosition>currentElementPosition-50){
            //add the animation className to fire the animation
              ani_ele.classList.add(`${animation[i]}-in`);
              ani_ele.classList.remove(`${animation[i]}`);

          }
      })  
    })
    
    //animation for home page
    const home__thing=document.getElementsByClassName("home__thing")[0];
    if (home__thing){
        let currentBottomPosition = currentScrollPosition + window.innerHeight;
        let currentElementPosition = getOffsetTop(home__thing)+home__thing.offsetHeight/2;
        if(currentBottomPosition>currentElementPosition-50){
            if (home__thing)
            activeThingDevice();
        }
    }
}

const activeThingDevice = () =>{
    document.getElementsByClassName("home-thing-bg-device-wrapper")[0].classList.add("home-thing-bg-device-wrapper-active");
    document.getElementsByClassName("home-thing-bg-device-left")[0].classList.add("home-thing-bg-left-active");
    document.getElementsByClassName("home-thing-bg-device-right")[0].classList.add("home-thing-bg-right-active");
    document.getElementsByClassName("home-thing-bg-device-padLock")[0].classList.add("home-thing-bg-padLock-active");
    document.getElementsByClassName("home-thing-bg-device-light-1")[0].classList.add("home-thing-bg-light-1-active");
    document.getElementsByClassName("home-thing-bg-device-light-2")[0].classList.add("home-thing-bg-light-2-active");
    document.getElementsByClassName("home-thing-bg-device-light-3")[0].classList.add("home-thing-bg-light-3-active");
}



const getOffsetTop = element => {
    let offsetTop = 0;
    while(element) {
      offsetTop += element.offsetTop;
      element = element.offsetParent;
    }
    return offsetTop;
  }






//Nav Bar
const controlNavBar =()=>{
    let currentScrollPosition = window.pageYOffset;
    if (preScrollPosition > currentScrollPosition ){
        //if page up, then down the nav bar
        downNavBar();
    } else {
        //if page down, then up the nav bar 
        upNavBar();
    }
    preScrollPosition = currentScrollPosition;

}

//if scroll up
const upNavBar = () => {
    let header = document.getElementsByTagName("header")[0];
    if (header){
    header.classList.add('nav-up');
    header.classList.remove('nav-down');
    }
};

// if scroll down
const downNavBar = () => {
    let header = document.getElementsByTagName("header")[0];
    if (header){
    header.classList.add('nav-down');
    header.classList.remove('nav-up');
    }

};
