import React from 'react';
import './App.scss';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';
import Home from "./pages/Home/Home";
import Navigation from "./components/Navigation/Navigation";
import ErrorPage from "./pages/Error/Error";
import './components/Animation/Animation.js';
import './components/Animation/Animation.scss';

export default class App extends React.Component {

  render() {
      return (
          <React.Fragment>
              <Router>
              <Navigation/>
              <Switch>
                  <Route component={Home}/>
                  {/* <Route component={ErrorPage}/> */}
              </Switch>
              </Router>
          </React.Fragment>
      );
  }
}

