import React from "react";
import {Link} from "react-router-dom"
import "./Error.scss";
import Footer from "../../components/Footer/Footer";

class Error extends React.Component {

    render() {
        return (
            <React.Fragment>
                <div className="page-not-found">
                    <div className="panel-page-not-found">
                        <h1>404 not found</h1>
                        <p>But we found this game in the meantime</p>
                        <Link to="/view-tasks" className="browse-button">
                            <div className="button-med button-cta">Browse Tasks</div>
                        </Link>
                    </div>
                </div>
                <Footer/>
            </React.Fragment>
        );
    }
}

export default Error;
