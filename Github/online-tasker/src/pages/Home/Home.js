import React from 'react';
import {Link} from 'react-router-dom';
import './Home.scss';
import Footer from "../../components/Footer/Footer"
import bg from '../../img/bg.jpg';
import hiw1 from '../../img/home-how-it-works-step-image-1.png'
import hiw2 from '../../img/home-how-it-works-step-image-2.png'
import hiw3 from '../../img/home-how-it-works-step-image-3.png'
import ThingDevice from '../../components/Thing/ThingDevice/ThingDevice'
import CardTask from '../../components/CardTask/CardTask';
export default class Home extends React.Component {

    render (){
        return (
            <main className="home home-page">
            <section className="home__container--banner">
                <div>
                    <h1>The best person for the job isn't always who you think</h1>
                    <p>Find the people with the skills you need on Onlinetasker</p>
                    <Link to="" className="browse-button">
                        <button className="button--lg banner__button  button--space-xs">Get started now</button>
                    </Link>
                </div>
                <img alt="Online-Tasker" className="img--full-size"
                     src={bg}/>
            </section>

            <section className="home__service home__container fade">
                <h2>What do you need done?</h2>

                <div className="home__service__items">
                
                    <div className="home__service__items__container">
                        <div className="home__service__item">
                            <svg width="24" height="24" viewBox="0 0 24 24"><path d="M15.18 10.9l-.38-.47a.76.76 0 0 0-.58-.28h-1.46v-2h.71a3.63 3.63 0 0 0 3.8-3.44 3.64 3.64 0 0 0-3.79-3.45H6.55A.76.76 0 0 0 5.8 2v1.66a1.87 1.87 0 0 0 1.93 1.79A1.37 1.37 0 0 1 9.2 6.67v.73a.75.75 0 0 0 .75.75h1.31v2H9.78a.76.76 0 0 0-.58.28l-.38.47a14.28 14.28 0 0 0-3.12 8.89 3 3 0 0 0 3 3h6.66a3 3 0 0 0 3-3 14.28 14.28 0 0 0-3.18-8.89zM7.73 4c-.25 0-.43-.15-.43-.29v-.96h6.18a2.15 2.15 0 0 1 2.29 2 2.14 2.14 0 0 1-2.3 1.94H10.7A2.86 2.86 0 0 0 7.73 4zm7.6 17.3H8.67a1.47 1.47 0 0 1-1.47-1.51 12.74 12.74 0 0 1 2.8-7.95l.15-.19h1.12v4a1.13 1.13 0 0 1-.33.79l-.75.75a.75.75 0 0 0 0 1.06.75.75 0 0 0 1.06 0l.75-.75a2.61 2.61 0 0 0 .77-1.85v-4h1.1l.15.19a12.74 12.74 0 0 1 2.79 8 1.47 1.47 0 0 1-1.48 1.41z"></path> </svg>
                        </div>
                        Clean 
                    </div>
                    <div className="home__service__items__container">
                        <div className="home__service__item">
                            <svg width="24" height="24" viewBox="0 0 24 24"><path d="M20 5.25h-3L16.06 3a2.73 2.73 0 0 0-2.55-1.71h-3A2.73 2.73 0 0 0 7.94 3L7 5.25H4A2.75 2.75 0 0 0 1.25 8v11A2.75 2.75 0 0 0 4 21.75h16A2.75 2.75 0 0 0 22.75 19V8A2.75 2.75 0 0 0 20 5.25zM9.33 3.53a1.25 1.25 0 0 1 1.16-.78h3a1.28 1.28 0 0 1 1.17.78l.7 1.72H8.62zM4 6.75h16A1.25 1.25 0 0 1 21.25 8v5.11h-3.14a2.24 2.24 0 0 0-4.22 0h-3.78a2.24 2.24 0 0 0-4.22 0H2.75V8A1.25 1.25 0 0 1 4 6.75zm12.75 7.12v2a.75.75 0 1 1-1.5 0v-2a.75.75 0 1 1 1.5 0zm-8 0v2a.75.75 0 1 1-1.5 0v-2a.75.75 0 1 1 1.5 0zM20 20.25H4A1.25 1.25 0 0 1 2.75 19v-4.39h3v1.26a2.25 2.25 0 1 0 4.5 0v-1.26h3.5v1.26a2.25 2.25 0 0 0 4.5 0v-1.26h3V19A1.25 1.25 0 0 1 20 20.25z"></path></svg>
                        </div>
                        Business
                    </div>
                    <div className="home__service__items__container">
                        <div className="home__service__item">
                            <svg width="24" height="24" viewBox="0 0 24 24"><path d="M22.74 9.16a5.08 5.08 0 0 0-5.07-5.08h-7.42a5.08 5.08 0 0 0-5 4.33h-3.2a.75.75 0 1 0 0 1.5h3.19a5.08 5.08 0 0 0 5 4.33h3.57v4.4a3.23 3.23 0 0 0 6.45 0V13.5a5 5 0 0 0 2.48-4.34zm-4 9.48a1.73 1.73 0 0 1-3.45 0v-4.4h2.35a5 5 0 0 0 1.1-.13zm-1.1-5.9h-7.39a3.58 3.58 0 0 1 0-7.16h7.42a3.58 3.58 0 0 1 0 7.16z"></path><path d="M18.28 8.41h-2.47a.75.75 0 1 0 0 1.5h2.47a.75.75 0 0 0 0-1.5z"></path></svg> 
                        </div>
                        Delivery
                    </div>    
                </div>


            </section>
            <section className="home__done">
                <div className="home__container fade">
                    <h2>See what others are getting done</h2>
                    <p>Got a few boxes to shift, an apartment or entire house? Get your home moved just the way you
                        want, by whom you want, when you want. Let OnlineTasker shoulder the load.</p>
                </div>
                <div className="home__container-animation fade">
                    <div className={"home__container--home-animation__inner animation--scroll-infinite"}>
                        <span> <CardTask/><CardTask/><CardTask/><CardTask/><CardTask/><CardTask/></span>   
                        <span> <CardTask/><CardTask/><CardTask/><CardTask/><CardTask/><CardTask/></span>    
                    </div>
                </div>
                <div className="home__container--browse-button">
                    <Link to="" className="browse-button">
                        <button className="button--lg">Get started now</button>
                    </Link>
                </div>
            </section>
            <section className="home__how-it-works home__container ">
                <div className="home__how-it-works--header fade">
                    <h2 className="clear-space">How does OnlineTasker work?</h2>
                    <p className="clear-space">Check out the video below to see exactly how Onlinetasker can help
                        you get those to-dos done once
                        and for all.</p>
                </div>
                
                <div className="home__how-it-works__video fade">
                  <iframe type="text/html" width="auto" height="auto" src="https://www.youtube.com/embed/Z0bEPpmB3PM?showinfo=0&controls=0&modestbranding=1&rel=0" title="1"></iframe>
                </div>

                <div className="home__how-it-works-boxes fade">
                    <div className="how-it-works_box">         
                        <img alt="" src={hiw1}></img>  
                        <div>
                            <h3>Post Your Task</h3>
                            <p>Tell us what you need. It's Free to post</p>
                        </div>        
                    </div>  
                    <div className="how-it-works_box">         
                        <img alt="" src={hiw2}></img>  
                        <div>
                            <h3>Review Offers</h3>
                            <p>Get offers from trusted Taskers and view profiles.</p>
                        </div>        
                    </div> 
                    <div className="how-it-works_box">         
                        <img alt="" src={hiw3}></img>  
                        <div>
                            <h3>Get it done</h3>
                            <p>Choose the right person for your task and get it done</p>
                        </div>        
                    </div>          
                </div>
                <div className="home__how-it-works-ready">
                    <h4>Ready to get that to-do list done?<br/>Start by posting a task today!</h4>
                    <Link to="" className="browse-button">
                        <button type="button" className="button--lg">Get started now</button>
                    </Link>
                </div>
            </section>
            <section className="home__thing">
                <div className="home__container fade">
                    <h2>Things you might also want to know</h2>
                    <p>Whether you’re getting work done or doing tasks on Shieldtasker, know that we’ve got your
                        back
                        every step of the way.</p>
                    <ThingDevice title="Top rated insurance"
                                     content="Insurance is there to ease any worries - making sure the Tasker has liability insurance from CGU while performing most task activities. T&C's apply."/>
                </div>

                <div className="home__how-it-works-ready fade">
                    <h4>Ready to get that to-do list done?<br/>Start by posting a task today!</h4>
                    <Link to="" className="browse-button">
                        <button type="button" className="button--lg fade">Get started now</button>
                    </Link>
                </div>

            </section>
            <Footer/>
        </main>
        )
    }


}



